import java.nio.charset.StandardCharsets;
import java.util.Date;

import com.google.common.hash.Hashing;

public class Block {
	private int index;
	private String previousHash;
	private Result data;
	private Date timestamp;
	private String hash;
	private int    honce; 

	public Block(int index, String previousHash, Result data) {
		super();
		this.index = index;
		this.previousHash = previousHash;
		this.data = data;
		this.timestamp = new Date();
		this.hash = generateHash();		
		this.honce = 0;
	}

	public String generateHash() {
		return Hashing.sha256().hashString("" + this.index + this.previousHash + this.data, StandardCharsets.UTF_8)
				.toString();
	}
	
	public String generateHashWithHonce() {
		return Hashing.sha256().hashString("" + this.index + this.previousHash + this.data + this.honce, StandardCharsets.UTF_8)
				.toString();
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(String previousHash) {
		this.previousHash = previousHash;
	}

	public Result getData() {
		return data;
	}

	public void setData(Result data) {
		this.data = data;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}		

	public int getHonce() {
		return honce;
	}

	public void setHonce(int honce) {
		this.honce = honce;
	}
	
	public void toMine(){
		//criar um hash com 2908 no in�cio
		do{
			this.hash = generateHashWithHonce();
			honce += 1;
		} while (!getHash().startsWith("1fce"));
	}

	@Override
	public String toString() {
		return "Block [index=" + index + ", previousHash=" + previousHash + ", data=" + data
				+ ", timestamp=" + timestamp + ", hash=" + hash + "]";
	}
}
