import java.util.ArrayList;
import java.util.List;

public class Blockchain {
	private List<Block> blocks = new ArrayList<Block>();
	private int index = 1;
	
	public void addBlock(Result result) throws CloneNotSupportedException {
		String hash = blocks.isEmpty() 
				? "0" 
				: getLastBlock().getHash();
		
		Result newResult = (Result) result.clone();
		
		Block block = new Block(index, hash, newResult);
		index++;
		
		block.toMine();		
		blocks.add(block);
	}
	
	public boolean isValid() {
		String previousHash = "0";
		for (Block b : blocks){
						
			if (!b.getPreviousHash().equals(previousHash) && 
			   (!b.getHash().equals(b.generateHash())))	
				return false;	
			
			previousHash = b.getHash();
			
		}
		return true;
			
	}
	
	public Block getLastBlock() {
		return blocks.get(blocks.size() - 1);
	}
	
	public Result getBalance() {
		Result r = new Result();
		r.setCandidatos(blocks.get(0).getData().getCandidatos());
		for (int i = 1 ; i < blocks.size(); i++){
			Block b = blocks.get(i);
			for(String s: r.getCandidatos().keySet())
				r.getCandidatos().put(s, b.getData().getCandidatos().get(s) + r.getCandidatos().get(s));
		}
		return r;
	}

	@Override
	public String toString() {
		String blockString = "";
		for (Block block : blocks) {
			blockString += block.toString() + "\n";
		}
		return "Blockchain [\n" + blockString + "]";
	}
}
