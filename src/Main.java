import java.util.Iterator;
import java.util.Random;

public class Main {
	public static void main(String[] args) throws CloneNotSupportedException {
		Blockchain blockchain = new Blockchain();				
		blockchain.addBlock(new Result());
		
		for (int i = 0; i < 10; i++) {
			Block b = blockchain.getLastBlock();
			Result r = b.getData();

			Iterator<String> it = r.getCandidatos().keySet().iterator();
			while(it.hasNext()) {
				Random rand = new Random();
			
				String key = it.next();
				r.getCandidatos().put(key, rand.nextInt(10));
			}
			
			blockchain.addBlock(r);
		}					
		
		System.out.println(blockchain);
		Result finalResult = blockchain.getBalance();
		System.out.println("Resultado final: \n" + finalResult);
		System.out.println("Testando se o hash � v�lido: " + blockchain.isValid());
	}
}
