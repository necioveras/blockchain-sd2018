import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Result implements Cloneable {
	private Map<String, Integer> candidatos = new HashMap<String, Integer>();
	
	public Result() {
		candidatos.put("Jo�o", 0);
		candidatos.put("Jos�", 0);
		candidatos.put("Maria", 0);
	}

	public Map<String, Integer> getCandidatos() {
		return candidatos;
	}

	public void setCandidatos(Map<String, Integer> candidatos) {
		this.candidatos = candidatos;
	}

	@Override
	public String toString() {
		String result = "";
		Iterator<String> it = candidatos.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			result += (key + ": " + candidatos.get(key) + "\n");
		}
		return "Result [\n" + result + "]";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Result result = new Result();
		
		Iterator<String> it = candidatos.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			result.getCandidatos().put(key, candidatos.get(key).intValue());
		}
		
		return result;
	}
}
